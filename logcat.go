// logcat
//
// Outputs syslog-formatted logs from a Logstash ES-backed index.
package main

import (
	"bufio"
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/olivere/elastic/v7"
)

var (
	elasticURL    = flag.String("url", getenv("ELASTICSEARCH_URL", "http://127.0.0.1:9200"), "Elasticsearch URL")
	fromDate      = flag.String("from", "-1h", "start `date`")
	toDate        = flag.String("to", "", "end `date` (default now, otherwise relative to --from)")
	doDumpQuery   = flag.Bool("dump-query", false, "don't do anything, just print the ES query as JSON")
	batchSize     = flag.Int("batch-size", 1000, "ES scroll batch `size`")
	indexFmt      = flag.String("index-format", "logstash-2006.01.02", "ES index `format` string, as supported by Go time.Format()")
	objType       = flag.String("obj-type", "_doc", "ES index object `type`")
	isoTimestamps = flag.Bool("iso", false, "dump ISO timestamps")
	jsonOutput    = flag.Bool("json", false, "output logs as JSONL")
)

func getenv(k, dflt string) string {
	if s := os.Getenv(k); s != "" {
		return s
	}
	return dflt
}

func usage() {
	// nolint: errcheck
	fmt.Fprintf(os.Stderr, `Usage: logcat [<options>] <query>...
Query a syslog database on Elasticsearch.

This tool can query Logstash-like archives stored on Elasticsearch
that contain syslog logs. Use the --url option to select the ES
server to talk to.

The command-line arguments should contain an Elasticsearch query,
in the standard QueryString syntax.

Known options:

`)
	flag.PrintDefaults()
}

var allowedTimeFormats = []string{
	time.RFC3339,
	time.RFC822,
	"2006/1/2 15:04:05",
	"2006/1/2 15:04",
	"2006/1/2",
}

func parseTime(base time.Time, s string) (t time.Time, err error) {
	var d time.Duration
	switch {
	case strings.HasPrefix(s, "+"):
		d, err = time.ParseDuration(s[1:])
		if err != nil {
			return
		}
		t = base.Add(d)
	case strings.HasPrefix(s, "-"):
		d, err = time.ParseDuration(s[1:])
		if err != nil {
			return
		}
		t = base.Add(-d)
	default:
		for _, f := range allowedTimeFormats {
			t, err = time.Parse(f, s)
			if err == nil {
				break
			}
		}
	}
	return
}

func parseTimeRange(fromStr, toStr string) (from time.Time, to time.Time, err error) {
	now := time.Now()

	if fromStr == "" {
		from = now.Add(-1 * time.Hour)
	} else {
		from, err = parseTime(now, fromStr)
		if err != nil {
			err = fmt.Errorf("error in --from: %v", err)
			return
		}
	}

	if toStr == "" {
		to = now
	} else {
		to, err = parseTime(from, toStr)
		if err != nil {
			err = fmt.Errorf("error in --to: %v", err)
			return
		}
	}

	if to.Before(from) {
		err = errors.New("--to comes before --from")
	}
	return
}

type logMessage struct {
	Timestamp time.Time `json:"@timestamp"`
	Message   string    `json:"message"`
	Host      string    `json:"host"`
	Tag       string    `json:"tag"`
}

func (m *logMessage) format() string {
	var ts string
	if *isoTimestamps {
		ts = m.Timestamp.Format(time.RFC3339)
	} else {
		ts = m.Timestamp.Format(time.Stamp)
	}
	if m.Tag == "" {
		return fmt.Sprintf("%s %s%s\n", ts, m.Host, m.Message)
	}
	return fmt.Sprintf("%s %s %s%s\n", ts, m.Host, m.Tag, m.Message)
}

func getIndexes(from, to time.Time) []string {
	var idxs []string
	t := time.Date(from.Year(), from.Month(), from.Day(), 0, 0, 0, 0, time.UTC)
	for t.Before(to) {
		idxs = append(idxs, t.Format(*indexFmt))
		t = t.AddDate(0, 0, 1)
	}
	return idxs
}

func makeQuery(q string, from, to time.Time) (elastic.Query, []string) {
	queries := []elastic.Query{
		elastic.NewMatchQuery("_type", *objType),
		elastic.NewRangeQuery("@timestamp").From(from).To(to),
	}
	if q != "" {
		queries = append(queries, elastic.NewQueryStringQuery(q))
	}

	query := elastic.NewBoolQuery().Must(queries...)
	indexes := getIndexes(from, to)
	return query, indexes
}

func messageToString(obj interface{}) string {
	var m logMessage
	if err := json.Unmarshal(obj.(json.RawMessage), &m); err != nil {
		log.Printf("error decoding message: %v", err)
		return ""
	}
	// Empty message logs are probably structured logs, skip them.
	if m.Message == "" {
		return ""
	}
	return m.format()
}

func runQuery(client *elastic.Client, query elastic.Query, indexes []string, w io.Writer) error {
	jsonCh := newBatchChannel(1000)
	outCh := newBatchChannel(1000)
	var wg sync.WaitGroup

	// Start workers, reading from jsonCh and sending output to outCh.
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		w := outCh.Sender()
		go func() {
			defer w.Close()
			defer wg.Done()
			jsonCh.Foreach(func(obj interface{}) {
				var s string
				if *jsonOutput {
					if enc, err := json.Marshal(obj); err == nil {
						s = string(enc) + "\n"
					}
				} else {
					s = messageToString(obj)
				}
				if s != "" {
					w.Push(s)
				}
			})
		}()
	}

	// Run the query, process the ES results and dispatch them to the jsonCh.
	jsonW := jsonCh.Sender()
	go func() {
		// Ordering is important here: close the jsonCh sender first.
		defer jsonCh.Close()
		defer jsonW.Close()

		scroll := client.Scroll(indexes...).
			Query(query).
			Sort("@timestamp", true).
			Size(*batchSize).
			KeepAlive("10m")

		for {
			result, err := scroll.Do(context.Background())
			if err == io.EOF {
				return
			} else if err != nil {
				log.Println(err)
				return
			}
			for _, hit := range result.Hits.Hits {
				jsonW.Push(hit.Source)
			}
		}
	}()

	// Synchronize.
	go func() {
		wg.Wait()
		outCh.Close()
	}()

	outCh.Foreach(func(obj interface{}) {
		io.WriteString(w, obj.(string)) // nolint
	})

	return nil
}

type batchChannel struct {
	ch chan []interface{}
	sz int
}

func newBatchChannel(sz int) *batchChannel {
	return &batchChannel{
		sz: sz,
		ch: make(chan []interface{}, 100),
	}
}

func (b *batchChannel) Close() {
	close(b.ch)
}

func (b *batchChannel) Foreach(f func(interface{})) {
	for buf := range b.ch {
		for _, obj := range buf {
			f(obj)
		}
	}
}

func (b *batchChannel) Sender() *batchSender {
	return &batchSender{
		ch:   b.ch,
		sz:   b.sz,
		wbuf: make([]interface{}, 0, b.sz),
	}
}

type batchSender struct {
	ch   chan []interface{}
	wbuf []interface{}
	sz   int
}

func (s *batchSender) Push(obj interface{}) {
	s.wbuf = append(s.wbuf, obj)
	if len(s.wbuf) >= s.sz {
		s.ch <- s.wbuf
		s.wbuf = make([]interface{}, 0, s.sz)
	}
}

func (s *batchSender) Close() {
	if len(s.wbuf) > 0 {
		s.ch <- s.wbuf
	}
}

func main() {
	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()

	from, to, err := parseTimeRange(*fromDate, *toDate)
	if err != nil {
		log.Fatalf("invalid time range: %v", err)
	}

	query, indexes := makeQuery(strings.Join(flag.Args(), " "), from, to)
	if *doDumpQuery {
		src, _ := query.Source()                    // nolint
		b, _ := json.MarshalIndent(src, "", "    ") // nolint
		fmt.Printf("indexes: %v\n", indexes)
		fmt.Printf("query:\n%s\n", string(b))
		return
	}

	client, err := elastic.NewClient(elastic.SetURL(*elasticURL))
	if err != nil {
		log.Fatal(err)
	}
	w := bufio.NewWriter(os.Stdout)
	if err := runQuery(client, query, indexes, w); err != nil {
		log.Fatal(err)
	}
	w.Flush() // nolint
}
