logcat
======

Dumps syslog-formatted entries from an Elasticsearch-backed Logstash
index.

It uses Elasticsearch's *scroll* API.


## Installation

Simply running:

    $ go get git.autistici.org/ai3/tools/logcat

will install the *logcat* binary in `$GOPATH/bin`.


## Usage

The *logcat* tool provides a few command-line switches to simplify
creating the final ES query:

* `--from` and `--to` allow you to select a time range (by default the
  last hour of logs will be selected)
* `--facility` filters results for a specific syslog facility ("mail",
  "kernel", etc).

Any other arguments on the command line will be parsed as a query
string, so you can do things like extracting logs for a specific
program:

    $ logcat --facility mail 'program:"postfix/smtpd"'

and other arbitrary queries using the Elasticsearch
[query string syntax](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#query-string-syntax).
